#include "packetreceiver.h"

// Конструктор класса
PacketReceiver::PacketReceiver(){
    waitingBinaryData = false;
    waitingTextData = false;
    savedData = NULL;
    savedDataSize = 0;
}

// Деструктор класса
PacketReceiver::~PacketReceiver(){
    // Очистка сохраненных данных
    if(savedDataSize) delete[] savedData;

    printf("Обработано пакетов: %d\n", binaryPackets.size() + textPackets.size());

    for(size_t i=0; i<binaryPackets.size(); ++i)
        delete[] binaryPackets.at(i);
    for(size_t i=0; i<textPackets.size(); ++i)
        delete[] textPackets.at(i);

    binaryPackets.clear();
    textPackets.clear();
}

// Интерфейс получения блока данных
void PacketReceiver::Receive(const char *data, unsigned int size){
    DBG_1 printf("\n\nПолучен блок данных (%d байт)\n", size);

    // Размер полученных данных
    unsigned int receivedSize = size;
    // Указатель на полученные данные
    const char *receivedData = data;
    // Номер последнего байта найденного пакета
    unsigned int lastByte = -1;
    // Указатель на позицию конца найденного пакета
    const char *pos = data;

    /* Если ожидается продолжение незаконченного пакета, то
     * присоединяем новые данные к уже сохраненнным,
     * переустанавливаем указатель на сохраненные данные,
     * изменяем размер данных и устанавливаем позицию в начале сохраненных данных
    */
    if(waitingTextData || waitingBinaryData){
        receivedData = appendSavedData(data, size);
        receivedSize = savedDataSize;
        pos = receivedData;
    }

    // Проверка каждого байта полученных данных
    for(size_t i=0; i<receivedSize; ++i){
        // Если обнаружен признак бинарного пакета
        if(receivedData[i]-'\0' == 0x24){
            // Если вместе с признаком пришёл и размер данных
            if(i+4 < receivedSize){
                // Получение размера бинарного пакета
                unsigned int packetSize = 0;
                memcpy(&packetSize, pos+1, 4);

                // Если бинарный пакет пришёл целиком
                if(i+4+packetSize < receivedSize){
                    // Отправка бинарного пакета потребителю, если пакет не пустой
                    if(packetSize) BinaryPacket(pos+5, packetSize);

                    // Если ожидалось продолжение бинарного пакета
                    if(waitingBinaryData){
                        // Вычисление количества байт, нехватавших до целого пакета
                        unsigned int bytesFromData = packetSize+5-(savedDataSize-size);
                        // Возвращение указателя на блок полученных данных и восстановление размера данных
                        receivedData = data;
                        receivedSize = size;
                        // Установка позиции в конец пакета в полученном блоке данных
                        pos = data + bytesFromData;
                        // Сохранение номера последнего байта найденного пакета
                        lastByte = bytesFromData-1;
                        // Удаление сохраненных данных
                        delete[] savedData;
                        savedData = NULL;
                        savedDataSize = 0;
                        waitingBinaryData = false;
                    }
                    // Если не ожидалось продолжение бинарного пакета
                    else {
                        // Сохранение номера последнего байта найденного пакета
                        lastByte = i+4+packetSize;
                        // Смещение позиции в конец пакета
                        pos += 5+packetSize;
                    }
                    // Пропуск уже обработанных байтов
                    i = lastByte;
                }
                // Если бинарный пакет не пришёл целиком
                else {
                    DBG_2 printf("\nБинарный пакет не пришёл целиком, ожидается новый блок c данными...\n");
                    // Сохранение оставшихся необработанных байтов, если не ожидались бинарные данные
                    // (в противном случае полученные данные уже сохранены перед началом цикла)
                    if(!waitingBinaryData)
                        appendSavedData(pos, receivedSize-i);

                    waitingBinaryData = true;
                    // Прерывание цикла, т.к. все данные уже сохранены
                    break;
                }
            }
            // Если размер бинарного пакета не пришёл целиком
            else {
                DBG_2 printf("\nБинарный пакет не пришёл целиком, ожидается новый блок c данными и их размером...\n");
                // Сохранение оставшихся необработанных байтов, если не ожидались бинарные данные
                // (в противном случае полученные данные уже сохранены перед началом цикла)
                if(!waitingBinaryData)
                    appendSavedData(pos, receivedSize-i);

                waitingBinaryData = true;
                // Прерывание цикла, т.к. все данные уже сохранены
                break;
            }
        }
        // Если обнаружен символ возврата каретки
        else if(receivedData[i] == '\r'){
            // Если достаточно байт для проверки признака конца текстового пакета (\r\n\r\n)
            if(i+3 < receivedSize){
                // Проверка остальных трёх символов на признаки конца текстового пакета
                if(receivedData[i+1] == '\n' && receivedData[i+2] == '\r' && receivedData[i+3] == '\n'){
                    // Вычисление размера текстового пакета
                    unsigned int packetSize = i-(lastByte+1);

                    // Если ожидалось продолжение текстового пакета
                    if(waitingTextData){
                        // Отправка текстового пакета потребителю, если пакет не пустой
                        if(packetSize)
                            TextPacket(savedData, packetSize);

                        // Вычисление количества байт, нехватавших до целого пакета
                        unsigned int bytesFromData = packetSize+4-(savedDataSize-size);
                        // Возвращение указателя на блок полученных данных и восстановление размера данных
                        receivedData = data;
                        receivedSize = size;
                        // Установка позиции в конец пакета в полученном блоке данных
                        pos = data + bytesFromData;
                        // Сохранение номера последнего байта найденного пакета
                        lastByte = bytesFromData-1;
                        // Удаление сохраненных данных
                        delete[] savedData;
                        savedData = NULL;
                        savedDataSize = 0;
                        waitingTextData = false;
                    }
                    // Если не ожидалось продолжение текстового пакета
                    else {
                        // Отправка текстового пакета потребителю, если пакет не пустой
                        if(packetSize) TextPacket(pos, packetSize);
                        // Сохранение номера последнего байта найденного пакета
                        lastByte = i+3;
                        // Смещение позиции в конец пакета
                        pos += packetSize+4;
                    }
                    // Пропуск уже обработанных байтов
                    i = lastByte;
                }
            }
            // Если недостаточно байт для проверки признака конца текстового пакета (\r\n\r\n)
            else {
                DBG_2 printf("\nТекстовый пакет не пришёл целиком, ожидается новый блок c данными...\n");
                // Сохранение оставшихся необработанных байтов, если не ожидались текстовые данные
                // (в противном случае полученные данные уже сохранены перед началом цикла)
                if(!waitingTextData) appendSavedData(pos, receivedSize-1-lastByte);
                waitingTextData = true;
                // Прерывание цикла, т.к. все данные уже сохранены
                break;
            }
        }
    }

    // Если номер последнего байта найденного пакета не совпадает с колиством байт полученных данных
    if(lastByte != receivedSize-1){
        // Если не ожидалось продолжение бинарного или текстового пакета
        if(!waitingBinaryData && !waitingTextData){
            DBG_2 printf("\nТекстовый пакет не пришёл целиком, ожидается новый блок c данными...\n");
            // Сохранение оставшихся необработанных байтов,
            appendSavedData(pos, receivedSize-1-lastByte);
            waitingTextData = true;
        }
    }
}

// Callback-интерфейс передачи бинарного пакета потребителю
void PacketReceiver::BinaryPacket(const char *data, unsigned int size){
    char *test = new char[size];
    memcpy(test, data, size);
//    delete[] test;
//    test = NULL;
    binaryPackets.push_back(test);

    // Вывод отладочной информации
    DBG_1 printf("\nБинарный пакет (%d байт):\n", size);

    DBG_1 printf("-------------------------\n");
    DBG_1 for(unsigned int i=0; i<size; ++i)
              fputc(data[i], stdout);
    DBG_1 printf("\n-------------------------\n");
}

// Callback-интерфейс передачи текстового пакета потребителю
void PacketReceiver::TextPacket(const char *data, unsigned int size){
    // Имитация работы с полученным сообщением
    char *test = new char[size];
    memcpy(test, data, size);
//    delete[] test;
//    test = NULL;
    textPackets.push_back(test);

    // Вывод отладочной информации
    DBG_1 printf("Текстовый пакет (%d байт):\n", size);

    DBG_1 printf("---------------------------\n");
    DBG_1 for(unsigned int i=0; i<size; ++i)
             fputc(data[i], stdout);
    DBG_1 printf("\n---------------------------\n");
}

// Метод добавления новых данных к сохраненным незаконченным данным
char* PacketReceiver::appendSavedData(const char *data, unsigned int size){
    // Выделение памяти под новый размер данных
    char *newData = new char[savedDataSize+size];

    // Пересохранение уже имеющихся сохраненных данных
    if(savedData) memcpy(newData, savedData, savedDataSize);

    // Сохранение новых данных
    memcpy(newData+savedDataSize, data, size);

    // Удаление выделенной памяти под предыдущие сохраненные данные
    if(savedDataSize) delete[] savedData;

    // Сохранение новых данных и их размера
    savedData = newData;
    savedDataSize += size;

    // Вывод отладочной информации
    DBG_2 printf("Сохранено (%d байт): ", savedDataSize);
    DBG_3 for(unsigned int i=0; i<savedDataSize; ++i)
              printf("%c",savedData[i]);

    return savedData;
}
