TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    packetreceiver.cpp

HEADERS += \
    packetreceiver.h

OTHER_FILES += \
    testData-1024b.txt
