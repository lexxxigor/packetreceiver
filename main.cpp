#include <iostream>
#include <cstring>
#include <stdio.h>
#include <ctime>
#include <math.h>

#include "packetreceiver.h"

#define BUF_SIZE 1024

using namespace std;

/* В файле packetreceiver.h можно включить или
 * выключить отладочную информацию
 ***************************************************
 * Вывод приложения на компьютере с Intel Core 2 Duo
 ***************************************************
 * =================================================
 * Время работы: 6,61 секунд
 * Пропускная способность: 1238,58 Мбит\с
 * Всего обработано: 1024,00 Мб
 * Обработано пакетов: 31457280
*/

int main(){
    setlocale(LC_ALL, "Russian");

    // Чтение из файла бинарных данных
    /* В тестовом файле 30 пакетов */
    FILE *fp = fopen("testData-1024b.txt", "rb");

    if(!fp){
        fputs("Ошибка открытия файла!\n", stderr);
        return 1;
    }

    // Создание объекта класса обработки потока данных
    PacketReceiver receiver;

    // Выделение памяти под блоки данных, отправляемых на обработку
    char *data = new char[BUF_SIZE];
    memset(data, 0, BUF_SIZE);

    size_t size = fread(data, 1, BUF_SIZE, fp);
    if(size <= 0){
        printf("\nОшибка чтения файла!\n");
        return 2;
    }
    // Закрытие файла
    fclose(fp);

    size_t totalSize = 0;
    size_t startTime = 0, end_time = 0, workTime = 0;
    size_t iterationsFor1GbData = ceil((1<<30)/double(BUF_SIZE));

    // Отправка блоков данных
    do {
        startTime = clock();
        receiver.Receive(data, size);
        end_time = clock();
        workTime += end_time - startTime;
        totalSize += size;
    } while (--iterationsFor1GbData);

    // Удаление выделенной памяти
    delete[] data;

    double seconds = workTime/1000.0;
    double capacity = (totalSize>>20)/seconds*8;
    printf("\n=========================================\n");
    printf("Время работы: %.2f секунд\n"
           "Пропускная способность: %.2f Мбит\\с\n"
           "Всего обработано: %.2f Мб\n",
           seconds, capacity, double(totalSize>>20));

    return 0;
}

