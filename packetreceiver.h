#ifndef PACKETRECEIVER_H
#define PACKETRECEIVER_H

#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

/*
 * DBG_1 - вывод наиболее важной информации для отладки
 * DBG_2 - вывод дополнительной информации для отладки
 * DBG_3 - вывод подробной информации для отладки
 * if(1) - включение вывода / if(0) - выключение вывода
 * !!! не включать при замере производительности !!!
*/
#define DBG_1 if(0)
#define DBG_2 if(0)
#define DBG_3 if(0)

using namespace std;

// Интерфейс получения блока данных
struct IReceiver
{
    virtual void Receive(const char* data, unsigned int size) = 0;
};

// Callback-интерфейс передачи данных потребителю
struct ICallback
{
    virtual void BinaryPacket(const char* data, unsigned int size) = 0;
    virtual void TextPacket(const char* data, unsigned int size) = 0;
};

// Класс обработки потока данных
class PacketReceiver : IReceiver, ICallback {
public:
    PacketReceiver();
    ~PacketReceiver();

    void Receive(const char *data, unsigned int size);
    void BinaryPacket(const char *data, unsigned int size);
    void TextPacket(const char *data, unsigned int size);
private:
    // Флаги ожидания продолжения незаконченного бинарного и текстового пакетов соотвественно
    bool waitingBinaryData, waitingTextData;
    // Указатель на сохраненные незаконченные данные
    char *savedData;
    // Размер сохраненных незаконченных данных
    unsigned int savedDataSize;
    vector<char*> binaryPackets, textPackets;

    // Метод добавления новых данных к сохраненным незаконченным данным
    char* appendSavedData(const char *data, unsigned int size);
};

#endif // PACKETRECEIVER_H
